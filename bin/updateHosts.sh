#!/bin/bash

updateHosts() {
  # Get all running containers
  containers=$(docker ps -aq)

  # Initialize an empty list of domains
  domains=""

  # Loop through each container
  for container in $containers; do
    labels=$(docker inspect --format '{{json .Config.Labels}}' $container | jq -r 'to_entries[] | "\(.key)=\(.value)"')
    for label in $labels; do
      if [[ $label =~ ^traefik\.http\.routers\..*\.rule=Host\(\`([^\`]+)\`\)$ ]]; then
        domain=${BASH_REMATCH[1]}
        domains="$domains $domain"
      fi
    done
  done

  # Remove duplicates from the list of domains
  domains=$(echo $domains | tr ' ' '\n' | sort | uniq)

  # Check if the list of domains has changed
  if [ "$domains" != "$(cat /tmp/traefik-docker-domains 2>/dev/null)" ]; then
    echo "$domains" > /tmp/traefik-docker-domains

    # Restore default hosts file, or create a backup if it doesn't exist
    if [ ! -f /etc/hosts.hostess.backup ]; then \
      cp /etc/hosts /etc/hosts.hostess.backup; \
    else \
      cp /etc/hosts.hostess.backup /etc/hosts; \
    fi

    # Call mkcert with the list of domains
    if [[ -n "$domains" ]]; then
      mkcert -cert-file ./certs/local.crt -key-file ./certs/local.key $domains
    fi

    # Add domains to /etc/hosts using hostess
    for domain in $domains; do
      hostess add $domain 127.0.0.1
    done

    docker restart boulzy-traefik-docker
  fi
}

# Watch Docker events
updateHosts
if [[ "$1" == "--watch" || "$1" == "-w" ]]; then
  docker events --filter 'type=container' --filter 'event=create' --filter 'event=start' --filter 'event=stop' --filter 'event=destroy' | while read event
  do
    # Handle the event
    updateHosts
  done
fi
