SHELL := bash
ifeq ($(shell uname),Windows_NT)
    SHELL := bash.exe
endif

.SHELLFLAGS := -euo pipefail -c
MAKEFLAGS += --warn-undefined-variables
MAKEFLAGS += --no-builtin-rules

OS?=$(shell uname)
ifeq ($(OS),Linux)
OS=linux
endif
ifeq ($(OS),Darwin)
OS=darwin
endif
ifeq ($(OS),Windows_NT)
OS=windows
endif
OS_ARCH?=$(shell uname -m)

.DEFAULT_GOAL:=help
.PHONY: help
help:
	@awk 'BEGIN {FS = ":.*##"; printf "\n\033[1mUsage:\033[0m\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-40s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' Makefile

##@ [Docker]

COMPOSE_FILE = compose.yaml
ifneq ($(wildcard compose.override.yaml),)
	COMPOSE_FILE := $(COMPOSE_FILE):compose.override.yaml
endif
COMPOSE = COMPOSE_FILE=$(COMPOSE_FILE) docker compose

.PHONY: build
build: ARGS ?=
build: SERVICE ?=
build: ## Build the Docker Compose services images
	@$(COMPOSE) build $(ARGS) $(SERVICE)

.PHONY: up
up: ARGS ?= --detach --remove-orphans --pull always
up: ## Start the Docker Compose services
	@$(COMPOSE) up $(ARGS)

.PHONY: stop
stop: ## Stop the Docker Compose services
	@$(COMPOSE) stop

.PHONY: down
down: ARGS ?= --volumes --remove-orphans
down: ## Stop and remove the Docker Compose services
down: stop
	@$(COMPOSE) down $(ARGS)

.PHONY: restart
restart: ## Restart the Docker Compose services
restart: stop up

.PHONY: reset
reset: ## Stop, remove, rebuild and restart the Docker Compose services
reset: down build up

.PHONY: logs
logs: ARGS ?= --follow
logs: ## Show the Docker Compose services logs
	@$(COMPOSE) logs $(ARGS)

##@ [Install]

.PHONY: install
install: ## Install the project
install: mkcert hostess network build up hosts

# @see https://github.com/FiloSottile/mkcert
.PHONY: mkcert
mkcert: ## Install mkcert and the local CA
ifeq ($(shell which mkcert),)
	@printf "You may be asked for your password to install mkcert\n"
	@sudo curl --location --output /usr/local/bin/mkcert "https://github.com/FiloSottile/mkcert/releases/download/v1.4.4/mkcert-v1.4.4-$(OS)-$(OS_ARCH)"
	@sudo chmod +x /usr/local/bin/mkcert
	@printf "mkcert installed\n"
endif
	@$$(which mkcert) -install
	@mkdir -p ./certs
# If you're using Firefox, you must add the rootCA.pem to the browser certificates.
# @see https://github.com/FiloSottile/mkcert/issues/370#issuecomment-1280377305

.PHONY: hostess
hostess: ## Install hostess
ifeq ($(shell which hostess),)
	@printf "You may be asked for your password to install hostess\n"
	@sudo curl --location --output /usr/local/bin/hostess "https://gitlab.com/boulzy/hostess/-/releases/0.5.2/downloads/binaries/hostess-$(OS)-$(OS_ARCH)"
	@sudo chmod +x /usr/local/bin/hostess
endif
	@printf "hostess installed\n"

.PHONY: network
network: ## Create local network
	@docker network inspect traefik >/dev/null 2>&1 || docker network create --scope global traefik

.PHONY: uninstall
uninstall: ## Uninstall the project
uninstall: down rm-network rm-hostess rm-mkcert

# @see https://github.com/FiloSottile/mkcert/issues/208
.PHONY: rm-mkcert
rm-mkcert: ## Uninstall mkcert and the local CA
ifneq ($(shell which mkcert),)
	@printf "You may be asked for your password to uninstall mkcert\n"
	@sudo $$(which mkcert) -uninstall || printf "Local CA not installed\n"
	@sudo rm -rf "$$(which mkcert -CAROOT)"
	@sudo rm -f $$(which mkcert)
	@rm -rf ./certs
	@printf "mkcert uninstalled\n"
else
	@printf "mkcert is not installed\n"
endif

.PHONY: rm-hostess
rm-hostess: ## Uninstall hostess
ifneq ($(shell which hostess),)
	@printf "You may be asked for your password to uninstall hostess\n"
	@if [ -f /etc/hosts.hostess.backup ]; then sudo cp /etc/hosts.hostess.backup /etc/hosts; rm -f /etc/hosts.hostess.backup; fi
	@sudo rm -f $$(which hostess)
	@printf "hostess uninstalled\n"
else
	@printf "hostess is not installed\n"
endif

.PHONY: rm-network
rm-network: ## Remove local network
	@docker network rm -f traefik

##@ [Usage]

.PHONY: hosts
hosts: ## Add domains to /etc/hosts
	@sudo ./bin/updateHosts.sh

.PHONY: watch
watch: ## Watch Docker containers for hosts updates
	@sudo ./bin/updateHosts.sh --watch
