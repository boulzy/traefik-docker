# boulzy/traefik-docker

Use [Traefik](https://doc.traefik.io/traefik/) as a reverse proxy for your
[Docker](https://docs.docker.com/get-started/overview/) containers.

-----

**boulzy/traefik-docker** is a small project used to start Traefik in a Docker
container on your host. With a little configuration, Traefik will route HTTP
traffic to your Docker containers, allowing you to use domain names and SSL for
local development.

## 🚀 Features

- Easy install
- Traefik UI
- Locally-trusted development certificates
- `/etc/hosts` management
- Docker network to share between the Traefik container and your containers

## ⚙️ Requirements

- [git](https://git-scm.com/)
- [Docker](https://docs.docker.com/get-docker/)
- [make](https://en.wikipedia.org/wiki/Make_(software))

> ℹ️ If they're not already present on your host, the following tools will be
> installed:
> 
> - [mkcert](https://github.com/FiloSottile/mkcert)
> - [hostess](https://github.com/cbednarski/hostess)

## ⬇️ Installation

Clone this repository in your workspace:

```shell
$ git clone git@gitlab.com:boulzy/traefik-docker.git
$ cd traefik-docker
```

Then, run the following command to setup Traefik and your local environment:

```shell
$ make install
```

The Traefik UI should be available at https://traefik.local.  
It's itself using Traefik to be exposed through this URL.

## 📚 Documentation

- [🌐 Traefik documentation](https://doc.traefik.io/traefik/getting-started/concepts/)
- [🐳 Traefik & Docker documentation](https://doc.traefik.io/traefik/providers/docker/)

### The Traefik container

After the installation, a Traefik container should be running on your host. It
will look for labels on running containers for configuration. The configuration
is dynamic, meaning that you don't need to restart the Traefik container to look
for changes in containers labels. Note that you need to restart the container
with the labels attached for them to be effective.

Traefik will listen for every local request on ports 80 and 443, and route them
to the configured containers. Your containers don't need to map ports to your
host, they may only expose them. Traefik will be able to access them using the
`traefik` network created during the installation.

### Configure domain names

To use domain names, you need your host to redirect requests for some domain
names to `http(s)://localhost` for Traefik to handle them.

This is done by editing the `/etc/hosts` file.
[hostess](https://github.com/cbednarski/hostess) is a CLI tool that ease the
management of hosts.

If you want to use SSL, you also need to generate a locally trusted certificate.
[mkcert](https://github.com/FiloSottile/mkcert) is a CLI tool allowing us to do
just that.

A `make` command is available to automate all this for us. It will look for
containers configured with Traefik labels and create a local certificat for them,
then add them in `/etc/hosts`.

```shell
$ make hosts
```

### Configure a Docker Compose project

Here is an example of a Docker Compose configuration file exposing a nginx server
through Traefik:

```yaml
# compose.yaml
version: '3.8'

services:
    nginx:
        image: nginx:1.25-alpine
        networks:
            - default
            - traefik
        labels:
            - "traefik.enable=true"
            - "traefik.http.routers.my-app.rule=Host(`www.my-app.local`)"
            - "traefik.http.routers.my-app.tls=true"
#            - "traefik.http.services.my-app.loadbalancer.server.port=80"
            - "traefik.docker.network=traefik"

networks:
    traefik:
        external: true
```

If the domain name `www.my-app.com` has been configured in the previous step,
the nginx container is now available at `https://www.my-app.com`. All traffic
from HTTP will be redirected to HTTPS.

> ℹ️ Because Traefik handle the HTTPS requests, the nginx server can be
> configured to listen for port 80 only, simplifying the configuration required.
> If your container exposes more than one port, you will need to tell Traefik
> on which port to redirect incoming requests.

## 💻 Usage

Run `make help` or `make` to see the available commands.

I suggest you to create aliases to easily use this project while working on
other projects:

```shell
# ~/.zshrc or ~/.bashrc
alias traefik="make -C ~/Workspace/traefik-docker hosts"
alias traefik-hosts="make -C ~/Workspace/traefik-docker hosts"
alias traefik-logs="make -C ~/Workspace/traefik-docker logs"
alias traefik-reset="make -C ~/Workspace/traefik-docker reset"
alias traefik-restart="make -C ~/Workspace/traefik-docker restart"
alias traefik-watch="make -C ~/Workspace/traefik-docker watch"
```
